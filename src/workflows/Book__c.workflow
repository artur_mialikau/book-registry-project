<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Book_is_sold_more_than_2000_times_notification</fullName>
        <description>Book is sold more than 2000 times notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>artur_mialikau@epam.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SoldBooksNotification</template>
    </alerts>
    <rules>
        <fullName>2000 Books sold Notification</fullName>
        <actions>
            <name>Book_is_sold_more_than_2000_times_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Book__c.Amount_Sold__c</field>
            <operation>greaterOrEqual</operation>
            <value>2000</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Test</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Book__c.Price__c</field>
            <operation>greaterThan</operation>
            <value>10</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
