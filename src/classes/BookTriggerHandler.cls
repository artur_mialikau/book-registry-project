public class BookTriggerHandler extends TriggerHandler {
    public BookTriggerHandler() {
    }

    public override void beforeInsert() {
        BookTriggerHandlerHelper.beforeInsertRevenueRecalculation((List<Book__c>)Trigger.new);
    }

    public override void beforeUpdate() {
        BookTriggerHandlerHelper.beforeUpdateRevenueRecalculation((Map<Id, Book__c>)Trigger.oldMap, (Map<Id, Book__c>)Trigger.newMap);
    }
}