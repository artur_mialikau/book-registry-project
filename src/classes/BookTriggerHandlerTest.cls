@IsTest
private class BookTriggerHandlerTest {

    @TestSetup
    static void setup(){
        Publishing_House__c publishingHouse = new Publishing_House__c(
                Name = 'TestPublishingHouse'
        );
        insert publishingHouse;

        Book__c book = new Book__c(
                Name = 'TestBook3',
                Title__c = 'Book 3',
                Publishing_House_del__c = publishingHouse.Id,
                Amount_Sold__c = 5,
                Price__c = 5
        );
        insert book;
    }

    @IsTest static void testRevenueCalculationOnBookInsert(){
        Book__c book = new Book__c(
                Name = 'TestBook1',
                Title__c = 'Book 1',
                Publishing_House_del__c = [
                        SELECT Id
                        FROM Publishing_House__c
                        WHERE Name = 'TestPublishingHouse'
                        LIMIT 1
                ].Id,
                Amount_Sold__c = 5,
                Price__c = 5
        );

        Test.startTest();
        insert book;
        Test.stopTest();

        List<Book__c> books = [
                SELECT Id, Name, Title__c, Publishing_House_del__c, Amount_Sold__c, Price__c, Revenue__c
                FROM Book__c
                WHERE Name = 'TestBook1'
                LIMIT 1
        ];
        Book__c resultBook = (books.size() == 1) ? books.get(0) : null;

        System.assertEquals(resultBook.Revenue__c, book.Price__c * book.Amount_Sold__c);
    }

    @IsTest static void testRevenueUpdateOnBookInsert(){
        Book__c book = new Book__c(
                Name = 'TestBook2',
                Title__c = 'Book 2',
                Publishing_House_del__c = [
                        SELECT Id
                        FROM Publishing_House__c
                        WHERE Name = 'TestPublishingHouse'
                        LIMIT 1
                ].Id,
                Amount_Sold__c = 5,
                Price__c = 5,
                Revenue__c = 20
        );

        Test.startTest();
        insert book;
        Test.stopTest();

        List<Book__c> books = [
                SELECT Id, Name, Title__c, Publishing_House_del__c, Amount_Sold__c, Price__c, Revenue__c
                FROM Book__c
                WHERE Name = 'TestBook2'
                LIMIT 1
        ];
        Book__c resultBook = (books.size() == 1) ? books.get(0) : null;

        System.assertEquals(resultBook.Revenue__c, book.Revenue__c + book.Price__c * book.Amount_Sold__c);
    }

    @IsTest static void testRevenueUpdateOnBookAmountSoldUpdate(){
        List<Book__c> books = [
                SELECT Id, Name, Title__c, Publishing_House_del__c, Amount_Sold__c, Price__c, Revenue__c
                FROM Book__c
                WHERE Name = 'TestBook3'
                LIMIT 1
        ];
        Book__c book = (books.size() == 1) ? books.get(0) : null;
        Integer diff = 5;
        book.Amount_Sold__c += diff;

        Test.startTest();
        update book;
        Test.stopTest();

        books = [
                SELECT Id, Name, Title__c, Publishing_House_del__c, Amount_Sold__c, Price__c, Revenue__c
                FROM Book__c
                WHERE Name = 'TestBook3'
                LIMIT 1
        ];
        Book__c resultBook = (books.size() == 1) ? books.get(0) : null;

        System.assertEquals(resultBook.Revenue__c, book.Revenue__c + diff * book.Price__c);
    }
}