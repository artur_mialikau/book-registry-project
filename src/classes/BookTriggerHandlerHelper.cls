public class BookTriggerHandlerHelper {
    public static void beforeInsertRevenueRecalculation(List<Book__c> books) {
        for (Book__c book : books) {
            if (book.Revenue__c != null) {
                book.Revenue__c += book.Amount_Sold__c * book.Price__c;
            } else {
                book.Revenue__c = book.Amount_Sold__c * book.Price__c;
            }
        }
    }

    public static void beforeUpdateRevenueRecalculation(Map<Id, Book__c> oldBooks,
            Map<Id, Book__c> newBooks) {
        for (Book__c newBook : newBooks.values()) {
            Book__c oldBook = oldBooks.get(newBook.Id);
            if (newBook.Amount_Sold__c != oldBook.Amount_Sold__c) {
                newBook.Revenue__c += (newBook.Amount_Sold__c - oldBook.Amount_Sold__c)
                        * newBook.Price__c;
            }
        }
    }
}